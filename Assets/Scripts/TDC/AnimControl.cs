﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimControl : MonoBehaviour
{
    private Animator animControl;

    private void Start()
    {
        animControl = GetComponent<Animator>();

        if(Random.Range(0, 101) > 50)
        {
            animControl.SetTrigger("one");
        }
        else
        {
            animControl.SetTrigger("two");
        }

        animControl.SetFloat("speed", Random.Range(0.85f, 1.25f));
    }
}
