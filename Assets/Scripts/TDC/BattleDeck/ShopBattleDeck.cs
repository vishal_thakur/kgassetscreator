﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopBattleDeck : MonoBehaviour
{
    public enum TEnter
    {
        Shop,
        Hand
    }

    public static ShopBattleDeck instance;
    public TEnter typeEnter;
    public List<CardUI> listShopCard = new List<CardUI>();

    [Header("Link")]
    public GameObject window;
    public Canvas parentCanvas;
    public Transform parentMoveCard;
    public Transform contentShop;
    public Transform contentHand;
    public List<Toggle> listToggleStep = new List<Toggle>();

    public CardUI exampleCard;

    public bool selected = false;
    public CardUI currentCard;
    public Transform awakeParent;

    #region Unity

    private void Awake()
    {
        instance = this;
        SelectedStep(DatabaseCard.indexStep);
        Initialization();
        Disable();
    }

    private void Update()
    {
        if (selected)
        {
            SelectedUpdate();
        }
    }

    #endregion

    #region Core

    public void Initialization()
    {
        print("Size: " + DatabaseCard.instance.GetCard().listHandCard.Count);

        for (int i = 0; i < listShopCard.Count; i++)
        {
            if (listShopCard[i])
            {
                Destroy(listShopCard[i].gameObject);
            }
        }

        listShopCard = new List<CardUI>();

        if (DatabaseCard.instance.GetCard().listHandCard.Count == 0)
        {
            GameObject newCad = Instantiate(exampleCard.gameObject, contentHand);
            newCad.SetActive(true);
            CardUI uiCard = newCad.GetComponent<CardUI>();
            uiCard.typeCard = CardUI.TCard.Shop;
            uiCard.Initialization(DatabaseCard.instance.listCard[0]);
            listShopCard.Add(uiCard);
            DatabaseCard.instance.GetCard().listHandCard.Add(DatabaseCard.instance.listCard[0]);
        }
        else
        {
            for (int i = 0; i < DatabaseCard.instance.GetCard().listHandCard.Count; i++)
            {
                GameObject newCad = Instantiate(exampleCard.gameObject, contentHand);
                newCad.SetActive(true);
                CardUI uiCard = newCad.GetComponent<CardUI>();
                uiCard.typeCard = CardUI.TCard.Shop;
                uiCard.Initialization(DatabaseCard.instance.GetCard().listHandCard[i]);
                listShopCard.Add(uiCard);
            }
        }

        for (int i = 0; i < DatabaseCard.instance.listCard.Count; i++)
        {
            if (!DatabaseCard.instance.GetCard().listHandCard.Contains(DatabaseCard.instance.listCard[i]))
            {
                GameObject newCad = Instantiate(exampleCard.gameObject, contentShop);
                newCad.SetActive(true);
                CardUI uiCard = newCad.GetComponent<CardUI>();
                uiCard.typeCard = CardUI.TCard.Shop;
                uiCard.Initialization(DatabaseCard.instance.listCard[i]);
                listShopCard.Add(uiCard);
            }
        }
    }

    #region Movement

    public void SelectedUnit(CardUI cardUI)
    {
        if (selected) { return; }

        selected = true;
        currentCard = cardUI;
        awakeParent = currentCard.transform.parent;
        currentCard.transform.SetParent(parentMoveCard);

        SetActiveCard(currentCard.gameObject, false);
    }

    public void SelectedUpdate()
    {
        Vector2 screenPoint;
        Vector3 inputSource = Input.mousePosition;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

#if UNITY_ANDROID
        foreach (Touch t in Input.touches)
        {
            if (t.phase == TouchPhase.Began || t.phase == TouchPhase.Moved)
            {
                inputSource = t.position;
                ray = Camera.main.ScreenPointToRay(t.position);
                break;
            }
        }
#endif

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            parentCanvas.transform as RectTransform,
            inputSource, parentCanvas.worldCamera,
            out screenPoint);

        currentCard.transform.position = parentCanvas.transform.TransformPoint(screenPoint);
    }

    public void SelectedEnd()
    {
        switch(typeEnter)
        {
            case TEnter.Shop:
                if (DatabaseCard.instance.GetCard().listHandCard.Count == 1)
                {
                    currentCard.transform.SetParent(contentHand);
                }
                else
                {
                    currentCard.transform.SetParent(contentShop);
                    DatabaseCard.instance.GetCard().listHandCard.Remove(currentCard.card);
                }
                break;
            case TEnter.Hand:
                currentCard.transform.SetParent(contentHand);
                DatabaseCard.instance.GetCard().listHandCard.Add(currentCard.card);
                break;
        }

        SetActiveCard(currentCard.gameObject, true);
        currentCard = null;
        selected = false;
    }

    private void SetActiveCard(GameObject target, bool value)
    {
        foreach (Image img in target.GetComponentsInChildren<Image>())
        {
            img.raycastTarget = value;
        }

        foreach (Text txt in target.GetComponentsInChildren<Text>())
        {
            txt.raycastTarget = value;
        }
    }

    public void EnterShop()
    {
        typeEnter = TEnter.Shop;
    }

    public void EnterHand()
    {
        typeEnter = TEnter.Hand;
    }

    public void SelectedStep(int index)
    {
        for (int i = 0; i < listToggleStep.Count; i++)
        {
            if(i == index)
            {
                listToggleStep[i].isOn = true;
            }
            else
            {
                listToggleStep[i].isOn = false;
            }
        }

        DatabaseCard.indexStep = index;
        Initialization();
    }

    #endregion

    #region Window

    public void Enable()
    {
        window.SetActive(true);
    }

    public void Disable()
    {
        window.SetActive(false);
    }

    #endregion

#endregion
}
