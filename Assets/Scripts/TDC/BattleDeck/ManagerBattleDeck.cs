﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

[System.Serializable]
public class FreezeCard
{
    public int index;
    public Transform center;
}

public class ManagerBattleDeck : MonoBehaviour
{
    public static ManagerBattleDeck instance;

    public Canvas parentCanvas;

    [Header("Card")]
    public RectTransform exampleCard;
    public List<CardUI> listCard = new List<CardUI>();
    public Transform parentContentCard;
    public Transform parentMoveCard;
    private Vector3 awakePositionCard;

    public int layerIndexCard;
    public HorizontalLayoutGroup layerGroupCard;

    [Header("Card")]
    public Image nextImgCard;
    public Text nextCostCard;
    public Text nextNameCard;

    [Header("Result Battle")]
    public GameObject parentResult;
    public GameObject parentWin;
    public GameObject parentLoss;
    public Text txtResultValue;
    public Color colorWin;
    public Color colorLoss;

    [Header("Mana")]
    public List<Image> listManaProperty = new List<Image>();
    public Color manaDisable;
    public Color manaEnable;

    [Header("Freeze")]
    public List<FreezeCard> listFreeze = new List<FreezeCard>();
    public Sprite freeSlot;
    public bool enterFreez;
    public int indexFeez;

    [Header("Deleted")]
    public bool enterDeleted = false;

    [Header("Grid")]
    public bool selected = false;
    public LayerMask ground;
    [HideInInspector] public LinkPath linkPath;
    public GameObject currentUnit;
    public CardUI currentCard;
    [HideInInspector] public DataCard nextCard;
    public GameObject parentBattle;

    [Header("Timer")]
    public Text txtBattleTimer;

    #region Unity

    private void Awake()
    {
        instance = this;
        exampleCard.gameObject.SetActive(false);
    }

    private void Start()
    {
        if (AIManager.instance)
        {
            AIManager.instance.parentTriggerZone.SetActive(false);
        }
    }

    private void Update()
    {
        //if(TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        //{
        //    if (!parentBattle.activeSelf)
        //    {
        //        parentBattle.SetActive(true);
        //        GameplayBattle.instance.resultBattle = GameplayBattle.ResultBattle.Processed;
        //    }
        //    if (!AIManager.instance.parentTowers.activeSelf)
        //    {
        //        Debug.LogError("bar bar!"); 
        //        AIManager.instance.parentTowers.SetActive(true);

        //        for(int i=0; i<6; i++)
        //        {
        //            RandomAddCard();
        //        }
        //    }

        //    ManaControl();
        //}
        //else if(TownLogic.typeScoutMode != TownLogic.TScoutMode.Battle)
        //{
        //    if (parentBattle.activeSelf) { parentBattle.SetActive(false); }
        //    if (AIManager.instance.parentTowers.activeSelf)
        //    {
        //        Debug.LogError("bar bar!");
        //        AIManager.instance.parentTowers.SetActive(false);
        //    }
        //}

        //if(selected)
        //{
        //    SelectedUpdate();
        //}
    }

    #endregion

    #region Core

    public void SelectedUnit(CardUI cardUI)
    {
        if (selected) { return; }

        layerGroupCard.enabled = false;
        layerIndexCard = cardUI.transform.GetSiblingIndex();

        selected = true;
        currentCard = cardUI;
        awakePositionCard = currentCard.transform.position;
        currentCard.transform.SetParent(parentMoveCard);
        currentUnit = Instantiate(cardUI.card.unit);
        currentUnit.SetActive(false);

        if (cardUI.card.typeCard == DataCard.TypeCard.Unit)
        {
            currentUnit.GetComponent<Collider>().enabled = false;
            currentUnit.GetComponent<NavMeshAgent>().enabled = false;
        }

        if (cardUI.card.typeCard == DataCard.TypeCard.Unit)
        {
            AIManager.instance.parentTriggerZone.SetActive(true);
        }

        SetActiveCard(currentCard.gameObject, false);
    }

    public void SelectedUpdate()
    {
        Vector2 screenPoint;
        Vector3 inputSource = Input.mousePosition;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

#if UNITY_ANDROID
        foreach(Touch t in Input.touches)
        {
            if(t.phase == TouchPhase.Began || t.phase == TouchPhase.Moved)
            {
                inputSource = t.position;
                ray = Camera.main.ScreenPointToRay(t.position);
                break;
            }
        }
#endif

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            parentCanvas.transform as RectTransform,
            inputSource, parentCanvas.worldCamera,
            out screenPoint);

        currentCard.transform.position = parentCanvas.transform.TransformPoint(screenPoint);

        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 100f , ground) && GetAllowedCard(hit, currentCard.card))
        {
            if (currentCard.card.typeCard == DataCard.TypeCard.Unit)
            {
                if (!currentUnit.activeSelf) { currentUnit.SetActive(true); }

                linkPath = hit.transform.GetComponent<LinkPath>();

                currentUnit.transform.position = hit.transform.position;
                print("Cast");
            }
            else
            {
                if (!currentUnit.activeSelf) { currentUnit.SetActive(true); }
                Vector3 fixPosition = hit.point;
                fixPosition.y = 0;
                currentUnit.transform.position = fixPosition;
            }
        }
        else
        {
            linkPath = null;
            if (currentUnit.activeSelf) { currentUnit.SetActive(false); }
        }
    }

    private bool GetAllowedCard(RaycastHit hit, DataCard card)
    {
        switch(card.typeCard)
        {
            case DataCard.TypeCard.Unit:
                if (hit.transform.tag == "TriggerSpawn") { return true; }
                break;

            case DataCard.TypeCard.SkillArea:
                return true; 
                break;
        }

        return false;
    }

    public void SelectedEnd()
    {
        if(DeletedControll())
        {
            RandomAddCard();
        }
        else if (FreezeControl())
        {
            currentCard.transform.SetParent(parentMoveCard);
        }
        else
        {
            if (!currentCard.freeze)
            {
                currentCard.transform.SetParent(parentContentCard);
                currentCard.transform.SetSiblingIndex(layerIndexCard);
            }
        }

        if (currentCard.card.typeCard == DataCard.TypeCard.Unit)
        {
            if (linkPath)
            {
                currentUnit.GetComponent<AIUnit>().Initialization(linkPath.indexParent);
                currentUnit.GetComponent<AIUnit>().aiSupport.currentStep = linkPath.transform.GetSiblingIndex();
                AIManager.instance.listAllyUnits.Add(currentUnit.GetComponent<AIUnit>());
                RemoveCard(currentCard);
                if (!currentCard.freeze) { RandomAddCard(); }
                GameplayBattle.instance.GetStateMana(DataMana.TType.Player).curMana -= currentCard.card.manaCost;
                print("Spawn");

                currentUnit.GetComponent<Collider>().enabled = true;
                currentUnit.GetComponent<NavMeshAgent>().enabled = true;
            }
            else
            {
                Destroy(currentUnit);
                print("Delete");
            }
        }
        else if (currentCard.card.typeCard == DataCard.TypeCard.SkillArea)
        {
            currentUnit.SetActive(true);

            if (currentUnit.GetComponent<SkillArea>()) { currentUnit.GetComponent<SkillArea>().StartSkill(); }
            if (currentUnit.GetComponent<SkillPosion>()) { currentUnit.GetComponent<SkillPosion>().StartSkill(); }
            if (currentUnit.GetComponent<SkillFreeze>()) { currentUnit.GetComponent<SkillFreeze>().StartSkill(); }


            RemoveCard(currentCard);
            if (!currentCard.freeze) { RandomAddCard(); }
            GameplayBattle.instance.GetStateMana(DataMana.TType.Player).curMana -= currentCard.card.manaCost;
        }

        if (currentCard) { SetActiveCard(currentCard.gameObject, true); }

        currentCard = null;
        linkPath = null;
        currentUnit = null;
        selected = false;
        AIManager.instance.parentTriggerZone.SetActive(false);
        awakePositionCard = Vector3.zero;
        layerGroupCard.enabled = true;
    }

    #region Card

    private void SetActiveCard(GameObject target, bool value)
    {
        foreach (Image img in target.GetComponentsInChildren<Image>())
        {
            img.raycastTarget = value;
        }

        foreach (Text txt in target.GetComponentsInChildren<Text>())
        {
            txt.raycastTarget = value;
        }
    }

    private void RandomAddCard()
    {
        if(!nextCard)
        {
            nextCard = DatabaseCard.instance.GetCard().listHandCard[Random.Range(0, DatabaseCard.instance.GetCard().listHandCard.Count)];
        }

        AddCard(nextCard);

        nextCard = DatabaseCard.instance.GetCard().listHandCard[Random.Range(0, DatabaseCard.instance.GetCard().listHandCard.Count)];
        UpdateNextCard();
    }

    private void AddCard(DataCard card)
    {
        GameObject newCard = Instantiate(exampleCard.gameObject, parentContentCard);
        newCard.SetActive(true);
        CardUI dataCard = newCard.GetComponent<CardUI>();

        dataCard.imgCard.sprite = card.visionSprite;
        dataCard.manaCost.text = card.manaCost.ToString();
        dataCard.txtName.text = card.unitName;
        dataCard.txtDescription.text = card.description;

        dataCard.card = card;
        listCard.Add(dataCard);
        dataCard.Initialization(card);

        UpdateNextCard();
    }

    private void RemoveCard(CardUI cardUI)
    {
        listCard.Remove(cardUI);
        Destroy(cardUI.gameObject);
        UpdateNextCard();
    }

    private void UpdateNextCard()
    {
        if (!nextCard) { return; }

        nextImgCard.sprite = nextCard.visionSprite;
        nextCostCard.text = nextCard.manaCost.ToString();
        nextNameCard.text = nextCard.unitName;
    }

    #endregion

    #region Freeze

    public void EnterFreeze(int index)
    {
        enterFreez = true;
        indexFeez = index;
        print("Enter");
    }

    public void ExitFreeze(int index)
    {
        enterFreez = false;
        print("Exit");
    }

    private bool FreezeControl()
    {
        if (!enterFreez)
        {
            currentCard.transform.position = awakePositionCard;
            return false;
        }

        if (!currentCard.freeze) { RandomAddCard(); }

        currentCard.transform.position = listFreeze[indexFeez].center.position;
        currentCard.freeze = true;
        currentCard.indexFeez = indexFeez;

        return true;
    }

    #endregion

    #region Mana

    private void ManaControl()
    {
        for (int i = 0; i < listManaProperty.Count; i++)
        {
            if(GameplayBattle.instance.GetStateMana(DataMana.TType.Player).curMana > i)
            {
                listManaProperty[i].color = manaEnable;
            }
            else
            {
                listManaProperty[i].color = manaDisable;
            }
        }

        for (int i = 0; i < listCard.Count; i++)
        {
            listCard[i].myButton.interactable = Mathf.FloorToInt(GameplayBattle.instance.GetStateMana(DataMana.TType.Player).curMana) >= listCard[i].card.manaCost;
        }
    }

    #endregion

    #region Deleted

    public void EnterDeleted()
    {
        enterDeleted = true;
    }

    public void ExitDeleted()
    {
        enterDeleted = false;
    }

    private bool DeletedControll()
    {
        if(enterDeleted)
        {
            bool getState = currentCard.freeze;
            RemoveCard(currentCard);
            return !getState;
        }
        else
        {
            return false;
        }
    }

    #endregion

    #region Result Battle

    public void SetResultBattle(GameplayBattle.ResultBattle result, int value)
    {
        parentResult.SetActive(true);

        string delimetr = string.Empty;

        switch(result)
        {
            case GameplayBattle.ResultBattle.Processed: break;
            case GameplayBattle.ResultBattle.Win:
                parentWin.SetActive(true);
                parentLoss.SetActive(false);

                delimetr = "+";
                txtResultValue.color = colorWin;
                break;
            case GameplayBattle.ResultBattle.Loss:
                parentWin.SetActive(false);
                parentLoss.SetActive(true);

                delimetr = "-";
                txtResultValue.color = colorLoss;
                break;
        }

        txtResultValue.text = delimetr + Mathf.Abs(value).ToString();
    }

    #endregion

    #endregion
}
