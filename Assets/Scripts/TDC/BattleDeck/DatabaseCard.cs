﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StepHandCard
{
    public List<DataCard> listHandCard = new List<DataCard>();
}

public class DatabaseCard : MonoBehaviour
{
    public static DatabaseCard instance;

    public List<DataCard> listCard = new List<DataCard>();

    public static int indexStep = 0;
    public static List<StepHandCard> listHandCard = new List<StepHandCard>();

    private void Awake()
    {
        instance = this;

        if (listHandCard.Count == 0)
        {
            for (int i = 0; i < 6; i++)
            {
                listHandCard.Add(new StepHandCard());
            }
            print("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII");
            print("Size: " + listHandCard.Count);
        }

        LoadListCard();
    }
    
    public StepHandCard GetCard()
    {
        return listHandCard[indexStep];
    }

    public void SaveListHandCard()
    {
        for (int i = 0; i < listHandCard.Count; i++)
        {
            PlayerPrefs.SetInt("CardHand" + i, listHandCard[i].listHandCard.Count);

            for (int w=0; 0<listHandCard[i].listHandCard.Count; i++)
            {
                PlayerPrefs.SetInt("CardHandUID" + i + w, listHandCard[i].listHandCard[w].uid);
            }
        }
    }

    public void LoadListCard()
    {
        for (int i = 0; i < listCard.Count; i++)
        {
            if (PlayerPrefs.GetInt("Card" + i) == 1 || listCard[i].uid == 2)
            {
                listCard[i].buy = true;
            }
            else
            {
                listCard[i].buy = false;
            }
        }
    }

    public void SaveListCard()
    {
        for (int i = 0; i < listCard.Count; i++)
        {
            if (listCard[i].buy || listCard[i].uid == 2)
            {
                PlayerPrefs.SetInt("Card" + i, 1);
            }
            else
            {
                PlayerPrefs.SetInt("Card" + i, 0);
            }
        }
    }
}
