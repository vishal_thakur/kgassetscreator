﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBot : MonoBehaviour
{
    public static AIBot instance;

    public List<DataCard> listCard = new List<DataCard>();

    [Header("Reaction")]
    [Range(0f,10f)] public float timeReaction;
    [Range(0f, 10f)] public float offsetReaction;
    public float pointReaction;
    [Range(0f, 10f)] public float curReaction;

    #region Unity

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Initialization();
    }

    private void Update()
    {
        CoreUpdate();
    }

    #endregion

    #region Core

    public void Initialization()
    {
        //if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        //{
        //    GameplayBattle.instance.GetStateMana(DataMana.TType.Bot);
        //    listCard = new List<DataCard>(DatabaseCard.instance.listCard);
        //}
    }

    public void CoreUpdate()
    {
        //if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        //{
        //    ReactionControl();
        //}
    }

    #region Reaction

    private void ReactionControl()
    {
        curReaction += Time.deltaTime;

        if(curReaction >= pointReaction)
        {
            curReaction = 0;
            pointReaction = Random.Range(timeReaction - offsetReaction, timeReaction + offsetReaction) + GetAgressionPart();
            Interaction();
        }
    }

    private void Interaction()
    {
        List<DataCard> switchCard = new List<DataCard>();

        for (int i = 0; i < listCard.Count; i++)
        {
            if(GameplayBattle.instance.GetStateMana(DataMana.TType.Bot).curMana >= listCard[i].manaCost)
            {
                switchCard.Add(listCard[i]);
            }
        }

        if(switchCard.Count > 0)
        {
            DataCard dataCard = switchCard[Random.Range(0, switchCard.Count)];
            var newEnemy = Instantiate(dataCard.unit);
            var dataEnemy = newEnemy.GetComponent<AIUnit>();
            dataEnemy.typeUnit = AIUnit.TUnit.Enemy;
            dataEnemy.aiSupport.invers = true;
            dataEnemy.Initialization(GetPart());
            AIManager.instance.listEnemyUnits.Add(dataEnemy);

            GameplayBattle.instance.GetStateMana(DataMana.TType.Bot).curMana -= dataCard.manaCost;
        }
    }

    private float GetAgressionPart()
    {
        List<int> counterBot = new List<int>();

        for (int i = 0; i < AIManager.instance.listAllyUnits.Count; i++)
        {
            counterBot.Add(AIManager.instance.listAllyUnits[i].aiSupport.indexParent);
        }

        int maxIndexBot = 0;
        int enemyUnitsBot = 0;

        for (int i = 0; i < counterBot.Count; i++)
        {
            int buffer = 0;

            for (int w = 0; w < counterBot.Count; w++)
            {
                if (counterBot[i] == counterBot[w])
                {
                    buffer++;
                }
            }

            if (buffer > maxIndexBot)
            {
                maxIndexBot = counterBot[i];
                enemyUnitsBot = buffer;
            }
        }

        //----------------------

        List<int> counter = new List<int>();

        for (int i = 0; i < AIManager.instance.listEnemyUnits.Count; i++)
        {
            counter.Add(AIManager.instance.listEnemyUnits[i].aiSupport.indexParent);
        }

        int maxIndex = 0;
        int playerUnits = 0;

        for (int i = 0; i < counter.Count; i++)
        {
            int buffer = 0;

            for (int w = 0; w < counter.Count; w++)
            {
                if (counter[i] == counter[w])
                {
                    buffer++;
                }
            }

            if (buffer > maxIndex)
            {
                maxIndex = counter[i];
                playerUnits = buffer;
            }
        }

        print("Line agr " + enemyUnitsBot);
        print("power agr " + (((playerUnits - enemyUnitsBot) * 0.8f)));
        return ((playerUnits - enemyUnitsBot) * 0.85f);
    }

    private int GetPart()
    {
        List<int> counter = new List<int>();

        for (int i = 0; i < AIManager.instance.listEnemyUnits.Count; i++)
        {
            counter.Add(AIManager.instance.listEnemyUnits[i].aiSupport.indexParent);
        }

        int maxIndex = 0;

        for (int i = 0; i < counter.Count; i++)
        {
            int buffer = 0;

            for (int w = 0; w < counter.Count; w++)
            {
                if(counter[i] == counter[w])
                {
                    buffer++;
                }
            }

            if(buffer > maxIndex)
            {
                maxIndex = counter[i];
            }
        }

        if (counter.Count > 0 && Random.Range(0, 101) <= 80)
        {
            return maxIndex;
        }
        else
        {
            return Random.Range(0, AIManager.instance.listData.Count);
        }
    }

    #endregion

    #endregion
}
