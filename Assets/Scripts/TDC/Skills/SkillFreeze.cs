﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillFreeze : SkillPlay
{
    public List<AIUnit> listUnits = new List<AIUnit>();

    public bool startSki = false;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void StartSkill()
    {
        base.StartSkill();
        startSki = true;
        StartCoroutine(DelayStartSkill());
    }

    private IEnumerator DelayStartSkill()
    {
        print("Start skill area");

        foreach (AIUnit unit in listUnits)
        {
            print("Start skill area find");

            if (unit.typeUnit == AIUnit.TUnit.Enemy)
            {
                if (unit.currentHealth > 0)
                {
                    unit.freeze = true;
                    unit.aiSupport.animControl.SetFloat("AnimSpeed", 0.0f);
                    print("Start skill area find damage");
                }
            }
        }

        yield return new WaitForEndOfFrame();
    }

    public override void EndSkill()
    {
        base.EndSkill();

        foreach (AIUnit unit in listUnits)
        {
            unit.freeze = false;
            unit.aiSupport.animControl.SetFloat("AnimSpeed", 1.0f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (startSkill) { return; }

        if (other.tag == "Unit")
        {
            AIUnit unit = other.GetComponent<AIUnit>();

            if (!listUnits.Contains(unit))
            {
                if (startSki) { StartCoroutine(DelayStartSkill()); }
                listUnits.Add(unit);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (startSkill) { return; }

        if (other.tag == "Unit")
        {
            AIUnit unit = other.GetComponent<AIUnit>();

            if (listUnits.Contains(unit))
            {
                listUnits.Remove(unit);
            }
        }
    }
}
