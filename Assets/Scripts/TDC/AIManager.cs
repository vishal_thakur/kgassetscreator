﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AIDataParent
{
    public int index;
    public Transform targetParent;
    public List<AIDataPoint> listPoints = new List<AIDataPoint>();

    public int indexEnemySpawn;
    public int indexAllySpawn;

    public Transform triggerSpawn;

    public void Initialization()
    {
        if (!targetParent) { return; }

        int indexRate = 0;

        foreach(Transform find in targetParent.GetComponentsInChildren<Transform>())
        {
            if(find != targetParent)
            {
                AIDataPoint newData = new AIDataPoint();

                newData.index = indexRate;
                newData.point = find;

                find.gameObject.AddComponent<SphereCollider>();
                SphereCollider colliderFind = find.GetComponent<SphereCollider>();
                colliderFind.radius = 2f;
                //colliderFind.isTrigger = true;

                find.transform.localScale = new Vector3(6, 2, 6);
                find.gameObject.AddComponent<LinkPath>();
                find.GetComponent<LinkPath>().indexParent = triggerSpawn.GetComponent<LinkPath>().indexParent;

                find.gameObject.tag = "TriggerSpawn";

                listPoints.Add(newData);

                indexRate++;
            }
        }
    }
}

[System.Serializable]
public class AIDataPoint
{
    [Header("Data")]
    public int index = 0;
    public Transform point;
}

public class AIManager : MonoBehaviour
{
    public static AIManager instance;
    public List<AIDataParent> listData = new List<AIDataParent>();

    public float timeBattle = 300;

    public List<AIUnit> listEnemyUnits = new List<AIUnit>();
    public List<AIUnit> listAllyUnits = new List<AIUnit>();

    public List<AIUnit> listEnemyTower;
    public List<AIUnit> listAllyTower;

    public GameObject parentTowers;
    public GameObject parentTriggerZone;

    public Transform rootCamera;

    #region Unity

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Initialization();
    }

    private void Update()
    {
        CoreUpdate();
    }

    #endregion

    #region Core

    private void Initialization()
    {
        //if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        //{
        //    for (int i = 0; i < listData.Count; i++)
        //    {
        //        listData[i].Initialization();
        //    }

        //    for (int i = 0; i < listAllyTower.Count; i++)
        //    {
        //        listAllyTower[i].Initialization(0);
        //    }

        //    for (int i = 0; i < listEnemyTower.Count; i++)
        //    {
        //        listEnemyTower[i].Initialization(0);
        //    }

        //    timeBattle = 300f;
        //}
    }

    private void CoreUpdate()
    {
        //if (Input.GetKey(KeyCode.LeftAlt))
        //{
        //    Cursor.visible = false;
        //    Cursor.lockState = CursorLockMode.Locked;
        //    //rootCamera.transform.Rotate(Vector3.up * Input.GetAxis("Mouse X"));
        //}
        //else
        //{
        //    if (!Cursor.visible)
        //    {
        //        Cursor.visible = true;
        //        Cursor.lockState = CursorLockMode.None;
        //    }
        //}

        //if (ManagerBattleDeck.instance && TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle && GameplayBattle.instance.resultBattle == GameplayBattle.ResultBattle.Processed)
        //{
        //    timeBattle -= Time.deltaTime;

        //    int minute = Mathf.FloorToInt(timeBattle / 60);
        //    int second = Mathf.Abs(Mathf.FloorToInt(minute * 60 - timeBattle));

        //    string txtSecond;

        //    if(second > 9)
        //    {
        //        txtSecond = second.ToString();
        //    }
        //    else
        //    {
        //        txtSecond = "0" + second;
        //    }

        //    if (timeBattle > 0)
        //    {
        //        ManagerBattleDeck.instance.txtBattleTimer.text = "0" + minute.ToString() + ":" + txtSecond;
        //    }
        //    else
        //    {
        //        ManagerBattleDeck.instance.txtBattleTimer.text = "00:00";
        //    }
        //    //Debug.LogError(timeBattle.ToString());

        //    if(timeBattle <= 0)
        //    {
        //        int healthAlly = 0;
        //        int healthEnemy = 0;

        //        for (int i = 0; i < listAllyTower.Count; i++)
        //        {
        //            healthAlly += listAllyTower[i].currentHealth;
        //        }

        //        for (int i = 0; i < listEnemyTower.Count; i++)
        //        {
        //            healthEnemy += listEnemyTower[i].currentHealth;
        //        }

        //        //Debug.LogError("Ally Health" + healthAlly);
        //        //Debug.LogError("Enemy Health" + healthEnemy);

        //        if (healthAlly > healthEnemy)
        //        {
        //            Debug.LogError("Win" + healthAlly);
        //            GameplayBattle.instance.CellResultBattle(GameplayBattle.ResultBattle.Win);
        //        }
        //        else
        //        {
        //            Debug.LogError("Loss" + healthEnemy);
        //            GameplayBattle.instance.CellResultBattle(GameplayBattle.ResultBattle.Loss);
        //        }
        //    }
        //    else
        //    {
        //        SpawnZoneControl();
        //    }
        //}
    }

    private void SpawnZoneControl()
    {
        int maxAllyIndex = 0;

        for (int i = 0; i < listAllyUnits.Count; i++)
        {
            if(listAllyUnits[i].aiSupport.currentStep > maxAllyIndex)
            {
                maxAllyIndex = listAllyUnits[i].aiSupport.currentStep;
            }
        }

        int maxEnemyIndex = 20;

        for (int i = 0; i < listEnemyUnits.Count; i++)
        {
            if (listEnemyUnits[i].aiSupport.currentStep < maxEnemyIndex)
            {
                maxEnemyIndex = listEnemyUnits[i].aiSupport.currentStep;
            }
        }

        for (int i = 0; i < listData.Count; i++)
        {
            for (int w = 1; w < listData[i].listPoints.Count; w++)
            {
                if (w < maxAllyIndex)
                {
                    listData[i].listPoints[w].point.gameObject.SetActive(true);
                }
                else
                {
                    listData[i].listPoints[w].point.gameObject.SetActive(false);
                }
            }

            listData[i].indexAllySpawn = Mathf.Clamp(maxAllyIndex - 1, 0, 20);
            if(listData[i].triggerSpawn) listData[i].triggerSpawn.position = listData[i].listPoints[listData[i].indexAllySpawn].point.position;

            listData[i].indexEnemySpawn = Mathf.Clamp(maxEnemyIndex + 1, 0, 20); 
        }
    }

    #endregion
}
