﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIView : MonoBehaviour
{
    public AIUnit aiUnit;

    private void Update()
    {
        for (int i = 0; i < aiUnit.listEnemyUnits.Count; i++)
        {
            if (aiUnit.listEnemyUnits[i] == null)
            {
                aiUnit.listEnemyUnits.RemoveAt(i);
                return;
            }

            if (aiUnit.listEnemyUnits[i].death)
            {
                aiUnit.listEnemyUnits.RemoveAt(i);
                return;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.parent && other.transform.parent.tag == "Unit")
        {
            AIUnit findUnit = other.transform.parent.GetComponent<AIUnit>();

            if(findUnit && findUnit.typeUnit != aiUnit.typeUnit && !findUnit.death && !aiUnit.listEnemyUnits.Contains(findUnit))
            {
                aiUnit.listEnemyUnits.Add(findUnit);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.parent && other.transform.parent.tag == "Unit")
        {
            AIUnit findUnit = other.transform.parent.GetComponent<AIUnit>();

            if (findUnit && aiUnit.listEnemyUnits.Contains(findUnit))
            {
                aiUnit.listEnemyUnits.Remove(findUnit);
            }
        }
    }
}
