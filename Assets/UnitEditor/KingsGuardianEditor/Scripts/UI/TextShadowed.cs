﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextShadowed : MonoBehaviour
{
    public string Value
    {
        get { return _text.text; }
        set { _text.text = value; _shadow.text = value; }
    }

    Text _shadow;
    Text _text;

    void Awake()
    {
        _shadow = GetComponent<Text>();
        foreach (var t in GetComponentsInChildren<Text>())
        {
            if (t != _shadow)
                _text = t;
        }
    }


}
