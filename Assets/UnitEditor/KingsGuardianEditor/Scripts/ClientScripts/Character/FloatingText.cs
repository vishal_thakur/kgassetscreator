﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class FloatingText : MonoBehaviour 
{
	public GameObject TextContainer;

	Text _text;
	Animator _animator;

	// Use this for initialization
	void Awake () 
	{
		_text = TextContainer.GetComponentInChildren<Text>();
		_animator = TextContainer.GetComponent<Animator>();	
		_text.enabled = false;
	}

    public void AddText(string text, CommandMessageColor color)
    {
        AddText(text, GetColor(color));
    }

	public void AddText(string text, Color color)
	{
		_animator.SetTrigger("Trigger");
		_text.enabled = true;
		_text.text = text;
		_text.color = color;
		Debug.Log("Invoke Remove Message");
		Invoke("RemoveMessage", 1.5f);
	}

	public void RemoveMessage()
	{
		_text.enabled = false;
	}


    Color GetColor( CommandMessageColor type)
    {
        Color c = Color.white;
        switch (type)
        {
            case CommandMessageColor.Red: c = Color.red; break;
            case CommandMessageColor.Green: c = Color.green; break;
            case CommandMessageColor.Yellow: c = Color.yellow; break;
        }

        return c;
    }
}