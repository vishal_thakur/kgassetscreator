﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class RaidStatusHUD : HUDBase
{
	public int HP 
	{
		get { return Mathf.RoundToInt(HPBar.value);} 
		set {HPBar.value = value; }
	}

	public int Shield 
	{
		get { return Mathf.RoundToInt(ShieldBar.value);} 
		set {ShieldBar.value = value; }
	}

	public Slider HPBar;
	public Slider ShieldBar;

	public void UpdateHPBar(float value, float max = 0)
	{
		if (!HPBar.gameObject.activeSelf)
			return;
		
		UpdateSlider(HPBar, value, max);
	}

	public void UpdateShieldBar(float value, float max = 0)
	{
		if (!ShieldBar.gameObject.activeSelf)
			return;

		ShieldBar.gameObject.SetActive(max == 0);

		if (max > 0)
			UpdateSlider(ShieldBar, value, max);
	}

	void UpdateSlider(Slider slider, float value, float max = 0)
	{
		if (max > 0)
			slider.maxValue = max;
		slider.value = value;     
	}

	protected override void OnToggle(bool show)
	{
		HPBar.gameObject.SetActive(show);
		ShieldBar.gameObject.SetActive(show);
	}
}
