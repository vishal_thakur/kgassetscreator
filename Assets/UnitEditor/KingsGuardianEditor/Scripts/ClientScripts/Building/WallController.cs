﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

public enum WallParts { MID, TOP, BOTTOM, LEFT, RIGHT }
public class WallController : MonoBehaviour
{   
	[System.Serializable]
	public class Wall
	{
		public WallParts Part;
		public GameObject Mesh;
	}
	public List<Wall> Walls = new List<Wall>();

	public void Toggle(WallParts part, bool visible)
	{
		var wall = Walls.Find(x => x.Part == part);
		if (wall != null)
			wall.Mesh.SetActive(visible);
	}
}
