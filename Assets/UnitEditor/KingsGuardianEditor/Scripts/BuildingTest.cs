﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuildingTest : MonoBehaviour
{
	public Camera MainCamera;
	public Camera SiegeCamera;

	public Text Upgrade;
	public GameObject SiegeTest;

	int _upgrade = 0;
	bool _working = true;
	bool _destroyed = false;
	bool _footprint = false;


	BuildingController _building;
	SiegeBuildingController _siege;

	void Start()
	{
		_building = GameObject.FindObjectOfType<BuildingController>();
		_siege = _building.GetComponent<SiegeBuildingController>();
		SiegeTest.SetActive(_siege != null);

		ChangeUpgrade(1);
	}

	public void ChangeUpgrade(int amount)
	{
		if (!_working)
		{
			_working = true;
			_building.ShowStand(_upgrade);
		}
		else
		{
			_working = false;
			_upgrade += amount;

			_building.ChangeModel(_upgrade);
			_building.HideStand();
		}

		_building.ToggleRuin(_destroyed);

		Upgrade.text = "Upgrade "+_upgrade+ ""+((_working) ? " (Working)":"");
	}

	public void ToggleFootprint()
	{
		_building.ToggleFootprint(_footprint);
		_footprint = !_footprint;
	}

	public void ColorFootprint(int color)
	{
		_building.ChangeColor((FootprintColor)color);
	}

	public void Destroy(Toggle toggle)
	{
		_destroyed = toggle.isOn;
		_building.ToggleRuin(toggle.isOn);
	}


	public void Attack(Transform target)
	{
		if (_siege != null)
			_siege.Attack(target.position + Vector3.up);
	}

	public void MoveTarget(Transform target)
	{
		
	}


	bool _siegeCam;
	public void ChangeCamera()
	{
		_siegeCam = !_siegeCam;
		MainCamera.enabled = !_siegeCam;
		SiegeCamera.enabled = _siegeCam;
	}
}

