﻿using UnityEngine;
using System.Collections;
using Runemark.AssetEditor;
using UnityEditor;
using System.IO;

public class KingsGuardiansEditorWindow : AssetEditorWindow 
{
	[MenuItem ("Window/KingsGuardiansEditor")]
	public static void  ShowWindow () {
		var w = (KingsGuardiansEditorWindow)EditorWindow.GetWindow(typeof(KingsGuardiansEditorWindow));
		w.Init();
	}

	protected override string scenePath { get { return _scenePath; } }
	string _scenePath = "Assets/UnitEditor/KingsGuardianEditor/Scenes/";

	protected override void OnInit()
	{

	}


	protected override void DrawMenu()
	{
		if (GUILayout.Button("Building Editor"))
			OpenEditor("BuildingEditor");
		
		if (GUILayout.Button("Character Editor"))
			OpenEditor("CharacterEditor");

		if (GUILayout.Button("[UTILITY]: Mass Refresh Parts"))
			MassRefreshPanel.Toggle(true);
	}

	protected override void OnEditorOpened(string editorName)
	{
		Debug.Log("Editor opened: " + editorName);
		BuildingEditorPanel.Toggle( editorName == "BuildingEditor" );
		CharacterEditorPanel.Toggle( editorName == "CharacterEditor" );
		MassRefreshPanel.Toggle(editorName == "MassRefreshPanel");
	}
}
