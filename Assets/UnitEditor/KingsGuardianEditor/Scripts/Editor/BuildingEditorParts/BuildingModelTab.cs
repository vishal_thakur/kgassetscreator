﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class BuildingModelTab : AssetEditorTabBase<BuildingEditorPanel>
{
	public BuildingHUD Hud;
	public RaidStatusHUD RaidHud;

	public float Scale = 1f;
	public List<Dictionary_Int_GameObject> Models = new List<Dictionary_Int_GameObject>();
	public List<Dictionary_Int_GameObject> Stands = new List<Dictionary_Int_GameObject>();
	public GameObject Ruin;
	public GameObject Base;
	public GameObject Footprint;
	public Vector2 ModelBaseSize = Vector2.zero;

	public BuildingModelTab(BuildingEditorPanel panel) : base (panel){}

	public override void Draw()
	{
		/*** SCALING ***/
		EditorGUILayout.BeginHorizontal();
		bool resize = false;
		Scale = EditorGUILayout.FloatField("Scale", Scale);
		if (GUILayout.Button("Rescale"))
		{
			resize = true;
			Ruin.transform.localScale = Vector3.one * Scale;
			Vector3 pos = Ruin.transform.position;
			pos.y = pos.y * Scale;
			Ruin.transform.position = pos;
		}

		if (GUILayout.Button("R"))
		{
			foreach (var a in panel.AssetList)
				a.Asset.transform.Rotate(new Vector3(0, 90, 0));
		}
		EditorGUILayout.EndHorizontal();


		/*** BASE SIZE ***/
		ModelBaseSize = EditorGUILayout.Vector2Field("Model Base Size:", ModelBaseSize);

		/*** HUDs ***/
		EditorGUILayout.BeginHorizontal();

		if (GUILayout.Button("Select HUD"))
		{
			if (Hud == null) CreateBuildingHUD(panel.AssetList[0].Asset.transform);			
			Selection.activeGameObject = Hud.gameObject;
		}
		if (GUILayout.Button("Select RaidHUD"))
		{
			if (RaidHud == null) CreateRaidHUD(panel.AssetList[0].Asset.transform);			
			Selection.activeGameObject = RaidHud.gameObject;
		}

		EditorGUILayout.EndHorizontal();

		/**************************************/
		EditorGUILayout.Space();
		CustomGUI.Splitter(2);
		EditorGUILayout.Space();

		EditorGUILayout.LabelField("Models");

		foreach (var m in Models)
		{
			if (resize)
			{
				m.Value.transform.localScale = Vector3.one * Scale;
				Vector3 pos = m.Value.transform.position;
				pos.y = pos.y * Scale;
				m.Value.transform.position = pos;
			}

			if(!DrawDictionary_Int_ModelField(m))
			{
				Models.Remove(m);
				return;
			}
		}
		if (GUILayout.Button("Add"))
			Models.Add(new Dictionary_Int_GameObject());
		
		CustomGUI.Splitter();

		EditorGUILayout.LabelField("Stands");
		foreach (var m in Stands)
		{
			if(!DrawDictionary_Int_ModelField(m))
			{
				Stands.Remove(m);
				return;
			}
		}
		if (GUILayout.Button("Add"))
			Stands.Add(new Dictionary_Int_GameObject());

		CustomGUI.Splitter();


		EditorGUILayout.BeginHorizontal();
		Ruin = (GameObject)EditorGUILayout.ObjectField("Ruin:", Ruin, typeof(GameObject), true);
		if(Ruin != null)
			Ruin.SetActive(EditorGUILayout.Toggle(Ruin.activeSelf));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		Base = (GameObject)EditorGUILayout.ObjectField("Base:", Base, typeof(GameObject), true);
		if(Base != null)
			Base.SetActive(EditorGUILayout.Toggle(Base.activeSelf));
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		Footprint = (GameObject)EditorGUILayout.ObjectField("Footprint:", Footprint, typeof(GameObject), true);
		if(Footprint != null)
			Footprint.SetActive(EditorGUILayout.Toggle(Footprint.activeSelf));
		EditorGUILayout.EndHorizontal();
	}

	public override void OnAssetInstantiated(Transform transform)
	{
		// Collect parts automatically.
		foreach (Transform t in transform)
		{        
			if (t.name == "Footprint")
				Base = t.gameObject;
			else
			{
				string[] name = t.name.Split('_');
				if (name.Length < 2)
					continue;

				if (name[1] == "R")
					Ruin = t.gameObject;
				else
				{
					int u = int.Parse(name[1]);
					if (name[0] == transform.name)
						Models.Add(new Dictionary_Int_GameObject(){ Key = u, Value = t.gameObject });
					else if (name[0].ToLower() == "upgrade")
						Stands.Add(new Dictionary_Int_GameObject(){ Key = u, Value = t.gameObject });
				}
			} 
		}

		// Create the footprint mesh
		Footprint = (GameObject)MonoBehaviour.Instantiate(Base, transform.position + Vector3.up * 0.1f, transform.rotation) as GameObject;
		Footprint.transform.SetParent(transform);

		CreateBuildingHUD(transform);
		CreateRaidHUD(transform);
	}


	void CreateBuildingHUD(Transform transform)
	{
		var hudPrefab = Resources.Load<GameObject>("BuildingHUD");
		var hudGO = (GameObject)MonoBehaviour.Instantiate(hudPrefab, transform.position+ Vector3.up * 0.05f, transform.rotation) as GameObject;
		Hud = hudGO.GetComponent<BuildingHUD>();
		Hud.transform.SetParent(transform);
	}

	void CreateRaidHUD(Transform transform)
	{
		var raidHudPrefab = Resources.Load<GameObject>("RaidStatusHUD");
		var raidHudGO = (GameObject)MonoBehaviour.Instantiate(raidHudPrefab, transform.position+ Vector3.up * 0.05f, transform.rotation) as GameObject;
		RaidHud = raidHudGO.GetComponent<RaidStatusHUD>();
		RaidHud.transform.SetParent(transform);
	}

	public override void Save(GameObject root)
	{
		foreach (var m in Models)
			m.Value.gameObject.SetActive(true);
		foreach (var m in Stands)
			m.Value.gameObject.SetActive(true);

		Ruin.SetActive(true);
		Base.SetActive(true);

		var data = root.AddComponent<BuildingController>();
		data.Base = Base;
		data.ModelBaseSize = ModelBaseSize;
		data.Models = Models;
		data.Ruin = Ruin;
		data.Stands = Stands;
		data.Footprint = Footprint;
		data.Hud = Hud;
		data.RaidHud = RaidHud;
	}

	public override void Load(GameObject root)
	{
		var data = root.GetComponent<BuildingController>();
		if (data == null)
			return;
		
		Base = data.Base;
		ModelBaseSize = data.ModelBaseSize;
		Models = data.Models;
		Ruin = data.Ruin;
		Stands = data.Stands;
		Footprint = data.Footprint;
		Hud = data.Hud;
		RaidHud = data.RaidHud;

		Scale = Ruin.transform.localScale.x;
	}

	public override void Reset()
	{
		Base = null;
		ModelBaseSize = Vector2.zero;
		Models = new List<Dictionary_Int_GameObject>();
		Ruin = null;
		Stands = new List<Dictionary_Int_GameObject>();
	}
}
