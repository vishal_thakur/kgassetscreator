﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class BuildingSiegeTab : AssetEditorTabBase<BuildingEditorPanel>
{
	public bool Enable;

	public GameObject LaunchPoint;		

	// Animation
	public bool AttackAnimation;

	// Attack FX (particle + sound)
	public bool EnableAttackFX;
	public GameObject AttackFX;
	public float DestroyTime = 0f;

	// Projectile
	public bool EnableProjectile;
	public GameObject Projectile;
	public float ProjectileSpeed = 1f;
	public bool BallisticPath;

	// Hit FX (particle + sound)
	public bool EnableHitFX;
	public GameObject HitFX;



	public BuildingSiegeTab(BuildingEditorPanel panel) : base (panel){}
	public override void Draw()
	{
		EditorGUI.BeginChangeCheck();
		Enable = EditorGUILayout.Toggle("Enable", Enable);
		if (EditorGUI.EndChangeCheck())
		{
			if (Enable && LaunchPoint == null)
			{
				LaunchPoint = new GameObject("LaunchPoint");
				LaunchPoint.transform.SetParent(panel.AssetList[0].Asset.transform);
			}
			if(!Enable && LaunchPoint != null)
			{
				LaunchPoint = null;
				EditorWindow.DestroyImmediate(LaunchPoint);
			}				
		}

		if (!Enable) return;

		CustomGUI.Splitter(1);

		if (GUILayout.Button("Select Launch Point"))
			Selection.activeGameObject = LaunchPoint.gameObject;


		AttackAnimation = EditorGUILayout.Toggle("Animation", AttackAnimation);
		if(AttackAnimation)
			EditorGUILayout.HelpBox("The attack animation should have \"OnAnimationCallback\" event!", MessageType.Warning);
		EditorGUILayout.Space();

		EnableAttackFX = EditorGUILayout.Toggle("Attack FX", EnableAttackFX);
		if (EnableAttackFX)
		{
			AttackFX = (GameObject)EditorGUILayout.ObjectField("Prefab", AttackFX, typeof(GameObject), true);
			if (AttackFX != null && !GameObject.Find(AttackFX.name))
				DestroyTime = EditorGUILayout.FloatField("Destroy time", DestroyTime);
			else
				DestroyTime = 0f;
			EditorGUILayout.HelpBox("Any sound effect should attached the the prefab!", MessageType.Info);
			EditorGUILayout.Space();
		}

		EnableProjectile = EditorGUILayout.Toggle("Projectile", EnableProjectile);
		if (EnableProjectile)
		{
			Projectile = (GameObject)EditorGUILayout.ObjectField("Prefab", Projectile, typeof(GameObject), false);
			ProjectileSpeed = EditorGUILayout.FloatField("Speed", ProjectileSpeed);
			BallisticPath = EditorGUILayout.Toggle("Ballistic Path", BallisticPath);
			EditorGUILayout.Space();
		}

		EnableHitFX = EditorGUILayout.Toggle("Hit FX", EnableHitFX);
		if (EnableHitFX)
		{
			HitFX = (GameObject)EditorGUILayout.ObjectField("Effect", HitFX, typeof(GameObject), false);
			EditorGUILayout.Space();
		}
	}

	public override void Save(GameObject root)
	{
		if (Enable)
		{
			var data = root.AddComponent<SiegeBuildingController>();

			data.AttackAnimation = AttackAnimation;
			data.EnableAttackFX = EnableAttackFX;
			data.EnableProjectile = EnableProjectile;
			data.EnableHitFX = EnableHitFX;

			data.DestroyTime = DestroyTime;
			data.ProjectileSpeed = ProjectileSpeed;
			data.BallisticPath = BallisticPath;

			data.LaunchPoint = (LaunchPoint != null) ? LaunchPoint.transform : null;
			data.AttackFX = AttackFX;
			data.Projectile = Projectile;
			data.HitFX = HitFX;
		}
	}

	public override void Load(GameObject root)
	{
		var data = root.GetComponent<SiegeBuildingController>();
		if (data == null)
		{
			Enable = false;
			return;
		}

		Enable = true;

		AttackAnimation = data.AttackAnimation;
		EnableAttackFX = data.EnableAttackFX;
		EnableProjectile = data.EnableProjectile;
		EnableHitFX = data.EnableHitFX;

		DestroyTime = data.DestroyTime;
		ProjectileSpeed = data.ProjectileSpeed;
		BallisticPath = data.BallisticPath;

		LaunchPoint = (data.LaunchPoint != null) ? data.LaunchPoint.gameObject : null;
		AttackFX = data.AttackFX;
		Projectile = data.Projectile;
		HitFX = data.HitFX;
	}

	public override void Reset()
	{
		Enable = false;
		AttackAnimation = false;
		EnableAttackFX = false;
		EnableProjectile = false;
		EnableHitFX = false;

		DestroyTime = 0f;
		ProjectileSpeed = 1f;
		BallisticPath = false;

		if(LaunchPoint != null && GameObject.Find(LaunchPoint.name))
			EditorWindow.DestroyImmediate(LaunchPoint);
		LaunchPoint = null;	

		if(AttackFX != null && GameObject.Find(AttackFX.name))
			EditorWindow.DestroyImmediate(AttackFX);
		AttackFX = null;

		if(Projectile != null && GameObject.Find(Projectile.name))
			EditorWindow.DestroyImmediate(Projectile);
		Projectile = null;
	
		if(HitFX != null && GameObject.Find(HitFX.name))
			EditorWindow.DestroyImmediate(HitFX);
		HitFX = null;
	}
}
