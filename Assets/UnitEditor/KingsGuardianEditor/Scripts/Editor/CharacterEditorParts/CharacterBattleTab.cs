﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CharacterBattleTab : AssetEditorTabBase<CharacterEditorPanel> 
{
	public BattleUnitStatUI Hud;

	bool _basicAttackSeqMoveCloser = true;

	public CharacterBattleTab(CharacterEditorPanel panel) : base (panel)
	{
		
	}

	public override void OnAssetInstantiated(Transform transform)
	{
		CreateStatusBar(transform);	
	}



	public override void Draw()
	{
		if (GUILayout.Button("Select HUD"))
		{
			if (Hud == null) CreateStatusBar(panel.AssetList[0].Asset.transform);			
			Selection.activeGameObject = Hud.gameObject;
		}

		EditorGUILayout.Foldout(true, "Basic Attack Sequence");
		EditorGUI.indentLevel++;

		_basicAttackSeqMoveCloser = EditorGUILayout.Toggle("Move Closer", _basicAttackSeqMoveCloser);

		EditorGUI.indentLevel--;
	}

	public override void Save(GameObject root)
	{
		var characterController = root.GetComponent<CharacterController>();
		characterController.BasicAttackSequence.MoveCloserFirst = _basicAttackSeqMoveCloser;
	}

	public override void Load(GameObject root)
	{
		var characterController = root.GetComponent<CharacterController>();
		_basicAttackSeqMoveCloser = characterController.BasicAttackSequence.MoveCloserFirst;

		Hud = root.GetComponentInChildren<BattleUnitStatUI>();
	}

	public override void Reset()
	{
		_basicAttackSeqMoveCloser = true;
		EditorWindow.DestroyImmediate(Hud);
	}


	void CreateStatusBar(Transform transform)
	{
		var hudPrefab = Resources.Load<GameObject>("BattleUnitStatUI");
		var hudGO = (GameObject)MonoBehaviour.Instantiate(hudPrefab, transform.position+ Vector3.up * 0.05f, transform.rotation) as GameObject;
		Hud = hudGO.GetComponent<BattleUnitStatUI>();
		Hud.transform.SetParent(transform);
	}
}
