﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

public class CharacterBasicTab : AssetEditorTabBase<CharacterEditorPanel> 
{	
	RuntimeAnimatorController _baseController;
	AnimatorOverrideController _animationController;
	Avatar _avatar;
	bool _meshCombiner = true;
	float _fSpeed = 1f;
	float _bSpeed = 1f;

	public CharacterBasicTab(CharacterEditorPanel panel) : base (panel)
	{
		_baseController = Resources.Load<RuntimeAnimatorController>("CharacterController");
	}

	public override void OnAssetInstantiated(Transform transform)
	{
		_animationController = new AnimatorOverrideController();
		_animationController.runtimeAnimatorController = _baseController;

		Animator a = transform.GetComponentInChildren<Animator>();
		_avatar = a.avatar;
		EditorWindow.DestroyImmediate(a);
	}


	public override void Draw()
	{
		if (_animationController == null)
			return;

		_meshCombiner = EditorGUILayout.Toggle("Combine Meshes", _meshCombiner);

		_fSpeed = EditorGUILayout.FloatField("Forward Speed", _fSpeed);
		_bSpeed = EditorGUILayout.FloatField("Backward Speed", _bSpeed);

		EditorGUILayout.Foldout(true, "Animations");
		EditorGUI.indentLevel++;	
		var animations = Enum.GetNames(typeof(AnimationEnums)).ToList();
		foreach( var name in animations)
			_animationController[name] = (AnimationClip)EditorGUILayout.ObjectField(name, _animationController[name], typeof(AnimationClip));
		EditorGUI.indentLevel--;

		// TEST enumerartion
		string missing = "";
		foreach (var c in _baseController.animationClips)
		{
			if(!animations.Contains(c.name))
				missing += "\n - " + c.name;		
		}
		if (missing != "") EditorGUILayout.HelpBox("These animations missing from the enum:" + missing, MessageType.Error);
	}


	public override void Save(GameObject root)
	{
		var animator = root.AddComponent<Animator>();
		animator.avatar = _avatar;
		animator.runtimeAnimatorController = _baseController;

		var characterController = root.GetComponent<CharacterController>();
		foreach (var clip in _animationController.clips)
			characterController.AnimationClips.Add( new CharacterController.OverrideAnimation(){Key = clip.originalClip.name, Value = clip.overrideClip});

		characterController.ForwardSpeed = _fSpeed;
		characterController.BackwardSpeed = _bSpeed;

		if (_meshCombiner)
		{
			var c = root.GetComponent<MeshCombiner>();
			if (c == null)
				root.AddComponent<MeshCombiner>();
		}

	}

	public override void Load(GameObject root)
	{
		_animationController = new AnimatorOverrideController();
		_animationController.runtimeAnimatorController = _baseController;

		var animator = root.GetComponent<Animator>();
		_avatar = animator.avatar;

		var characterController = root.GetComponent<CharacterController>();
		foreach (var clip in characterController.AnimationClips)
			_animationController[clip.Key] = clip.Value;



		_fSpeed = characterController.ForwardSpeed;
		_bSpeed = characterController.BackwardSpeed;

		var c = root.GetComponent<MeshCombiner>();
		_meshCombiner = c != null;
	}

	public override void Reset()
	{
		_animationController = null;
		_avatar = null;
		_meshCombiner = true;
	}
}
