﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CharacterRaidTab : AssetEditorTabBase<CharacterEditorPanel> 
{
	// TODO: Add 'Raid Abilities: particles'
	public RaidStatusHUD RaidHud;




	public CharacterRaidTab(CharacterEditorPanel panel) : base (panel)
	{

	}

	public override void OnAssetInstantiated(Transform transform)
	{
		CreateStatusBar(transform);
	}



	public override void Draw()
	{
		if (GUILayout.Button("Select HUD"))
		{
			if (RaidHud == null) CreateStatusBar(panel.AssetList[0].Asset.transform);			
			Selection.activeGameObject = RaidHud.gameObject;
		}
	}

	public override void Save(GameObject root)
	{

	}

	public override void Load(GameObject root)
	{
		RaidHud = root.GetComponentInChildren<RaidStatusHUD>();
	}

	public override void Reset()
	{
		EditorWindow.DestroyImmediate(RaidHud);
	}


	void CreateStatusBar(Transform transform)
	{
		var raidHudPrefab = Resources.Load<GameObject>("RaidStatusHUD");
		var raidHudGO = (GameObject)MonoBehaviour.Instantiate(raidHudPrefab, transform.position+ Vector3.up * 0.05f, transform.rotation) as GameObject;
		RaidHud = raidHudGO.GetComponent<RaidStatusHUD>();
		RaidHud.transform.SetParent(transform);
	}

}
