﻿using UnityEngine;
using System.Collections;
using Runemark.AssetEditor;
using UnityEditor;
using System.Collections.Generic;

public class BuildingEditorPanel : AssetEditorPanel<BuildingEditorPanel>
{
	public List<AssetInfo> AssetList{ get { return Assets; }}

	protected override string PanelTitle{ get { return "Building Editor"; }}
	protected override string category{ get { return "Building"; }}

	GameObject _prefab;

	int _tab = 0;
	string[] _tabLabels  = new string[]{ "Model", "Siege" };
	BuildingModelTab _modelTab;
	BuildingSiegeTab _siegeTab;

	protected override void Init()
	{
		_modelTab = new BuildingModelTab(this);
		_siegeTab = new BuildingSiegeTab(this);
	}

	protected override void OnDrawGUI()
	{
		if (Assets.Count == 0 && _prefab == null) return;

		if (_modelTab == null || _siegeTab == null)
			Initialize();

		CustomGUI.Splitter();
		_tab = GUILayout.SelectionGrid(_tab, _tabLabels, 2);

		CustomGUI.Splitter();

		switch (_tab)
		{
			case 0: _modelTab.Draw(); break;
			case 1: _siegeTab.Draw(); break;
		}

		CustomGUI.Splitter();

		if (_prefab == null && GUILayout.Button("Create Test"))
			_prefab = CreatePrefab();
		if (_prefab != null && GUILayout.Button("Undo Test"))
			UndoPrefab(_prefab);
	}

	protected override void OnAssetInstantiated(Transform transform)
	{
		if (_modelTab != null)
			_modelTab.OnAssetInstantiated(transform);
		if (_siegeTab != null)
			_siegeTab.OnAssetInstantiated(transform);
	}

	protected override void OnSave(GameObject root)
	{
		if (_modelTab != null)
			_modelTab.Save(root);
		if (_siegeTab != null)
			_siegeTab.Save(root);
	}

	protected override void OnLoad(GameObject root)
	{
		if (_modelTab != null)
			_modelTab.Load(root);
		if (_siegeTab != null)
			_siegeTab.Load(root);
	}

	protected override void OnReset()
	{
		if (_modelTab != null)
			_modelTab.Reset();
		if (_siegeTab != null)
			_siegeTab.Reset();
	}
}
