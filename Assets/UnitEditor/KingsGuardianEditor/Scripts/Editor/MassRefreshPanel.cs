﻿using UnityEngine;
using System.Collections;
using Runemark.AssetEditor;
using UnityEditor;
using System.Collections.Generic;

public class MassRefreshPanel : EditorWindow
{
	public static void Toggle(bool t)
	{
		var w = (MassRefreshPanel)EditorWindow.GetWindow(typeof(MassRefreshPanel));		
		w.position = new Rect((Screen.width - 250) / 2, (Screen.height - 400) / 2, 500, 400);
		if (!t)
			w.Close();
		else
			w.ShowUtility();
	}

	GameObject _hudPrefab;
	const string PREFAB_PATH = "Assets/_Prefabs/";

	void OnGUI () 
	{
		if (_hudPrefab == null)
			_hudPrefab = Resources.Load<GameObject>("BuildingHUD");


		if (GUILayout.Button("Refresh Building HUD"))
		{
			var namesAsset = AssetLoaderUtils.LoadAsset<AssetNames>("Assets/_Prefabs/Building/assetNames.prefab");
			List<string> names = (namesAsset != null) ? namesAsset.Names : new List<string>();

			RefreshBuildingHUD("Assets/_Prefabs/Building/" + names[0] + ".prefab");

			/*foreach( var n in names)
				RefreshBuildingHUD("Assets/_Prefabs/Building/" + n + ".prefab");*/
		}
	}


	void RefreshBuildingHUD(string path)
	{
		var go = (GameObject)Instantiate(AssetLoaderUtils.LoadAsset<GameObject>(path), Vector3.zero, Quaternion.identity) as GameObject;
		go.name = go.name.Replace("(Clone)", "");

		var oldHud = go.GetComponentInChildren<BuildingHUD>();
		Vector3 pos = oldHud.transform.position;
		Quaternion rot = oldHud.transform.rotation;
		DestroyImmediate(oldHud.gameObject);

		var newHud = (GameObject)MonoBehaviour.Instantiate(_hudPrefab) as GameObject;
		newHud.name = newHud.name.Replace("(Clone)", "");
		newHud.transform.position = pos;
		newHud.transform.rotation = rot;
		newHud.transform.SetParent(go.transform);

		// SAVE
		var data = go.GetComponent<AssetData>();
		data.LastSave =  System.DateTime.Now.ToString();
		PrefabUtility.CreatePrefab(path, go);

		Debug.Log(go.name + " Building HUD Refresh done");

		//DestroyImmediate(go);
	}



}