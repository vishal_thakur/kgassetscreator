﻿using UnityEngine;
using System.Collections;
using Runemark.AssetEditor;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Linq;

public class CharacterEditorPanel : AssetEditorPanel<CharacterEditorPanel> 
{
	// TESTS
	// Animation test

	public List<AssetInfo> AssetList{ get { return Assets; }}

	protected override string PanelTitle{ get { return "Character Editor"; }}
	protected override string category{ get { return "Character"; }}

	GameObject _prefab;

	int _tab = 0;
	string[] _tabLabels  = new string[]{ "Basic", "Battle", "Raid" };

	CharacterBasicTab _animationTab;
	CharacterBattleTab _battleTab;
	CharacterRaidTab _raidTab;


	protected override void Init()
	{
		_animationTab = new CharacterBasicTab(this);
		_battleTab = new CharacterBattleTab(this);
		_raidTab = new CharacterRaidTab(this);
	}

	protected override void OnAssetInstantiated(Transform transform)
	{
		if (_animationTab != null)
			_animationTab.OnAssetInstantiated(transform);
		if (_battleTab != null)
			_battleTab.OnAssetInstantiated(transform);
		if (_raidTab != null)
			_raidTab.OnAssetInstantiated(transform);
	}

	protected override void OnDrawGUI()
	{
		if (Assets.Count == 0) return;

		if (_animationTab == null )
			Initialize();

		CustomGUI.Splitter();
		_tab = GUILayout.SelectionGrid(_tab, _tabLabels, 3);
		CustomGUI.Splitter();

		switch (_tab)
		{
			case 0: _animationTab.Draw(); break;
			case 1:	_battleTab.Draw(); break;
			case 2:	_raidTab.Draw(); break;
		}

		CustomGUI.Splitter();

		if (_prefab == null && GUILayout.Button("Create Test"))
			_prefab = CreatePrefab();
		if (_prefab != null && GUILayout.Button("Undo Test"))
			UndoPrefab(_prefab);
	}


	protected override void OnSave(GameObject root)
	{
		var characterController = root.AddComponent<CharacterController>();

		if (_animationTab != null)
			_animationTab.Save(root);
		if (_battleTab != null)
			_battleTab.Save(root);
		if (_raidTab != null)
			_raidTab.Save(root);
	}

	protected override void OnLoad(GameObject root)
	{	
		var characterController = root.GetComponent<CharacterController>();

		if (_animationTab != null)
			_animationTab.Load(root);
		if (_battleTab != null)
			_battleTab.Load(root);
		if (_raidTab != null)
			_raidTab.Load(root);
	}

	protected override void OnReset()
	{
		if (_animationTab != null)
			_animationTab.Reset();
		if (_battleTab != null)
			_battleTab.Reset();
		if (_raidTab != null)
			_raidTab.Reset();
	}
}
