﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTest : MonoBehaviour
{
	public Camera camera1;
	public Camera camera2;

	public GameObject Target;
	CharacterController _character;
	Vector3 _origin;
	Quaternion _originRot;

	public int _position = 0; // 0 - origin, 1 - target, -1 moving


	bool _camera;



	void Start()
	{
		_character = GameObject.FindObjectOfType<CharacterController>();
		_character.Init();
		_origin = _character.transform.position;
		_originRot = _character.transform.rotation;
		_character.OnDestinationReached = OnDestinationReached;
		_character.OnAction = OnAction;
		_character.OnActionFinished = OnActionFinished;
		_character.OnActionStarted = OnActionStarted;
	
	}

	public void Reset()
	{
		_character.Stop();
		_character.transform.position = _origin;
		_character.transform.rotation = _originRot;
		_sequenceTest = false;
		_sequenceStep = -1;
		_position = 0;
	}

	public void SwitchCamera()
	{
		_camera = !_camera;
		camera1.enabled = !_camera;
		camera2.enabled = _camera;
	}


	public void Move()
	{
		var dest = (_position == 0) ? Target.transform.position : _origin;
		_character.Move(dest, _position == 1);
		if(_character.CurrentAction == ActionID.MoveForward || _character.CurrentAction == ActionID.MoveBackward)
			_position = -1;
	}

	public void Attack()
	{
		_character.BasicAttack();
	}

	public void CastOffense()
	{
		_character.CastOffensive();
	}

	public void CastBuff()
	{
		_character.CastBuff();
	}

	public void TakeDamage()
	{
		_character.OnHit();
	}

	bool _dead;
	public void ToggleDead()
	{
		_dead = !_dead;
		_character.SetDead(_dead);
		_character.OnHit();
	}


	#region Sequence Test
	bool _sequenceTest = false;
	int _sequenceStep = -1; // 0: move closer, 1: attack, 2: move back


	public void TestSequence()
	{
		Debug.Log("Basic Attack Sequence Test Started");
		NextSequence();
		_sequenceTest = true;
	}

	void NextSequence()
	{
		_sequenceStep++;
		if (!_character.BasicAttackSequence.MoveCloserFirst)
		{
			if (_sequenceStep == 0 || _sequenceStep == 2)
				_sequenceStep++;
		}

		switch (_sequenceStep)
		{
			case 0:	
				Debug.Log("Sequence " + _sequenceStep + ": Move closer");
				_character.Move(Target.transform.position);
				break;
			case 1:	
				Debug.Log("Sequence " + _sequenceStep + ": Basic Attack");
				_character.BasicAttack();
				break;
			case 2: 
				Debug.Log("Sequence " + _sequenceStep + ": Move back");
				_character.Move(_origin, true);
				break;
			case 3: 
				_sequenceTest = false;
				_sequenceStep = -1;
				Debug.Log("Basic Attack Sequence Test Finished");
				break;
		}
	}

	#endregion


	void OnDestinationReached(ActionID current)
	{
		if (_sequenceTest)
			NextSequence();
		else
		{
			_position = (current == ActionID.MoveBackward) ? 0 : 1;
			if (current == ActionID.MoveBackward)
				Reset();
		}
	}

	void OnActionStarted(ActionID current)
	{
		Debug.Log("OnActionStarted (" + current + ")");
	}

	void OnAction(ActionID current)
	{
		Debug.Log("OnAction (" + current + ")");
	}

	void OnActionFinished(ActionID current)
	{
		Debug.Log("OnActionFinished (" + current + ")");
		if (_sequenceTest)
			NextSequence();		
	}


	// Move Test: run, walk, walkb
	// Attack
	// Cast Buff
	// Cast Off
	// OnHit
	// Dead
	// Attack Sequence

	// HUD test (level, hp, shield, buffs, readiness)

}
