﻿using UnityEngine;
using System.Collections;

public class BuildingEditorHelper : MonoBehaviour 
{
	public float TileSize = 2f;
	public Vector2 GridSize = new Vector2(10, 10);

	void OnDrawGizmos()
	{
		Gizmos.color = Color.white;

		int xSize = Mathf.RoundToInt((float)GridSize.x / 2f);
		int ySize = Mathf.RoundToInt((float)GridSize.y / 2f);

		for (int x = -xSize; x <= xSize; x++)
		{
			float xPos = 0 + x * TileSize;
			Vector3 from = new Vector3(xPos, 0, -ySize * TileSize);
			Vector3 to = new Vector3(xPos, 0, ySize * TileSize);
			Gizmos.DrawLine(from, to);
		}

		for (int y = -ySize; y <= ySize; y++)
		{
			float yPos = 0 + y * TileSize;
			Vector3 f = new Vector3(-xSize* TileSize, 0, yPos);
			Vector3 t = new Vector3(xSize * TileSize, 0, yPos);
			Gizmos.DrawLine(f, t);
		}




		/*Color c = Color.cyan;
		c.a = 0.4f;
		Gizmos.color = c;

		foreach (var maptile in MapTilePositions)     
			Gizmos.DrawCube(maptile, new Vector3(TileSize, 0.05f, TileSize));*/
	}
}
