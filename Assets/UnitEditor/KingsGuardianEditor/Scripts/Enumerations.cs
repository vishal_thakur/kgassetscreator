﻿public enum ActionID : int 
{ 
	None = 0, 
	BasicAttack = 1,
	Buff = 2, 
	Offensive = 3,
	MoveForward = 4, 
	MoveBackward = 5,	
	Jump = 6
}

public enum CommandMessageColor : int
{
	White,
	Red,
	Green,
	Yellow,
}