﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using Runemark.EditorExtension;

namespace Runemark.AssetEditor
{
	public class AssetEditorPanel<T> : EditorWindow, IModalWindow  where T : AssetEditorPanel<T>
	{
		protected virtual string PanelTitle { get { return "Asset Editor Panel"; }}
		protected virtual string category { get { return "unknown"; }}

		protected GUIStyle style { get; private set; }
		protected Transform container { get; private set; }

		protected string id { get; private set; }
		protected string displayName { get; private set; }
		protected Texture2D icon { get; private set; }
		protected List<AssetInfo> Assets = new List<AssetInfo>();
		protected System.DateTime lastSave { get; private set; }

		bool _initialized;



		public static void Toggle(bool t)
		{
			if (t)
			{
				var w = (T)EditorWindow.GetWindow(typeof(T));
				w.position = new Rect(10, 10, 350, 800);
				w.Initialize();
				w.style = null;

				var go = GameObject.Find("_CONTAINER");
				if (go != null)
					w.container = go.transform;
				w.Clear();
			}
			else
			{
				var w = (T)EditorWindow.GetWindow(typeof(T));
				w.Close();
			}
		}


		protected void Initialize()
		{
			Init();
			_initialized = true;
		}

		void OnGUI()
		{
			if (!_initialized)
				Initialize();

			InitalizeStyle();

			GUILayout.Space(20);

			id = EditorGUILayout.TextField("ID", id);
			displayName = EditorGUILayout.TextField("Display Name", displayName);

			EditorGUILayout.BeginHorizontal();
			icon = (Texture2D)EditorGUILayout.ObjectField("Icon", icon, typeof(Texture2D), false);
			if (GUILayout.Button("Take", GUILayout.Width(50)))
				CreateIcon();
			
			EditorGUILayout.EndHorizontal();

			GUILayout.Space(10);

			foreach (var a in Assets)
			{
				if (a.AssetLoader == null)
				{
					Assets.Remove(a);
					return;
				}

				GUILayout.BeginHorizontal();
				if (GUILayout.Button("X"))
				{
					a.OnRemove();
					Assets.Remove(a);
					return;
				}

				EditorGUILayout.LabelField( (a.Asset != null) ? a.Asset.name : "Empty");

				if (GUILayout.Button("SELECT"))
					Selection.activeGameObject = a.Asset;

				if (GUILayout.Button("Browse"))
					InstantiateAsset(a, EditorUtility.OpenFilePanel("Browse", "Assets/Imports", "prefab"));       

				GUILayout.EndHorizontal();
			}

			if (GUILayout.Button("Add Asset Loader Object"))
				Assets.Add(new AssetInfo(container));
			
			OnDrawGUI();

			EditorGUILayout.BeginHorizontal();

			if (GUILayout.Button("NEW")) 
				Reset();
			if (GUILayout.Button("LOAD"))
				Load();
			if (GUILayout.Button("SAVE"))
				Save();

			EditorGUILayout.EndHorizontal();

			GUILayout.Box("Last Save: \n "+((lastSave != default(System.DateTime)) ? lastSave.ToString("u") : "never"), GUILayout.Width(300), GUILayout.Height(50));    
		}

		void Reset()
		{
			foreach (var a in Assets) 
				a.OnRemove();
			Assets.Clear();

			var go = GameObject.Find("PREFAB");
			DestroyImmediate(go);

			id = "";
			displayName = "";
			icon = null;

			lastSave = default(System.DateTime);

			OnReset();
		}

		protected void CreateIcon()
		{
			if (!AssetDatabase.IsValidFolder("Assets/_Icons"))
				AssetDatabase.CreateFolder("Assets", "_Icons" );

			if (!AssetDatabase.IsValidFolder("Assets/_Icons/"+category))
				AssetDatabase.CreateFolder("Assets/_Icons", category );

			string path = Application.dataPath + "/_Icons/" + category + "/" + id + "_icon.png";
			Screenshot.Screenshot.Take(path, 512, 512);		
			AssetDatabase.Refresh();
			icon = AssetLoaderUtils.LoadAsset<Texture2D>(path);
		}

		protected GameObject CreatePrefab()
		{
			if (Assets.Count == 0)
			{
				Debug.LogError("Nothing to save");
				return null;
			}  

			var root = new GameObject("PREFAB");
			root.transform.position = Vector3.zero;

			var data = root.AddComponent<AssetData>();
			data.ID = id;
			data.DisplayName = displayName;
			data.Icon = icon;

			OnSave(root);

			foreach (var a in Assets)
			{
				a.Asset.transform.SetParent(root.transform);
				data.Assets.Add(a.Asset);
				DestroyImmediate(a.AssetLoader);
			}
			Assets.Clear();

			return root;
		}

		protected void UndoPrefab(GameObject model)
		{
			AssetData data = model.GetComponent<AssetData>();

			foreach (var m in data.Assets)
			{
				var a = new AssetInfo(container);
				a.Asset = m;
				Assets.Add(a);
			}

			id = data.ID;
			displayName = data.DisplayName;
			icon = data.Icon;
			lastSave = (data.LastSave != "") ? System.Convert.ToDateTime(data.LastSave) : default(System.DateTime);

			OnLoad(model);
			DestroyImmediate(model);
		}

		void Save()
		{
			var prefab = CreatePrefab();

			lastSave = System.DateTime.Now;
			var data = prefab.GetComponent<AssetData>();
			data.LastSave = lastSave.ToString();

			string filename = data.ID.Replace(' ', '_');
			if (!AssetDatabase.IsValidFolder("Assets/_Prefabs"))
				AssetDatabase.CreateFolder("Assets", "_Prefabs" );

			if (!AssetDatabase.IsValidFolder("Assets/_Prefabs/"+category))
				AssetDatabase.CreateFolder("Assets/_Prefabs", category );

			string path = "Assets/_Prefabs/"+category;
			PrefabUtility.CreatePrefab(path+"/"+ filename + ".prefab", prefab); 

			UpdateNames(data.ID, path+"/assetNames.prefab");
		}


		void UpdateNames(string assetName, string path)
		{
			var namesAsset = AssetLoaderUtils.LoadAsset<AssetNames>(path);
			if (namesAsset == null)
			{
				namesAsset = ScriptableObject.CreateInstance<AssetNames>();
				AssetDatabase.CreateAsset(namesAsset, path);	
			}

			if (!namesAsset.Names.Contains(assetName))
			{
				namesAsset.Names.Add(assetName);
				EditorUtility.SetDirty(namesAsset);
			}
		}


		void Load()
		{
			var namesAsset = AssetLoaderUtils.LoadAsset<AssetNames>("Assets/_Prefabs/" + category + "/assetNames.prefab");
			List<string> names = (namesAsset != null) ? namesAsset.Names : new List<string>();
			AssetLoaderModal.Create(this, "Select an asset ("+category+")", names.ToArray());		
		}

		void Load(string name)
		{
			GameObject prefab = AssetLoaderUtils.LoadAsset<GameObject>("Assets/_Prefabs/" + category + "/" + name+".prefab");

			if (prefab != null)
			{		
				Reset();
				GameObject model = (GameObject)Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
				UndoPrefab(model);
			}
		}

		protected void Clear()
		{
			if (Assets.Count == 0 && container != null)
			{
				for (int cnt = 0; cnt < container.childCount; cnt++)
				{
					var t = container.GetChild(cnt);
					if (t.name == "AssetLoader")
						DestroyImmediate(t.gameObject);
				}
			}
		}


		protected virtual void Init(){}
		protected virtual void OnDrawGUI(){}
		protected virtual void OnReset(){}

		/// <summary>
		/// Add your data component to the root, and sets its parameters!
		/// </summary>
		/// <param name="root">Root.</param>
		protected virtual void OnSave(GameObject root){}
		protected virtual void OnLoad(GameObject root){}

		protected virtual void OnAssetInstantiated(Transform transform) { }



		void InitalizeStyle()
		{
			if (style == null)
			{
				style = new GUIStyle(GUI.skin.box);
				style.normal.background = TextureMaker.Create(2,2, new Color(.75f, .75f, .75f, 1f));
				style.normal.textColor = Color.black;
			}
		}

		void InstantiateAsset(AssetInfo a, string path)
		{       
			var go = (GameObject)Instantiate(AssetLoaderUtils.LoadAsset<GameObject>(path), Vector3.zero, Quaternion.identity) as GameObject;
			a.Asset = go;
			go.name = go.name.Replace("(Clone)", "");

			id = go.name;
			displayName = go.name;

			OnAssetInstantiated(go.transform);
		}

		/*T LoadAsset<T>(string path) where T : Object
		{
			if (path == "") return null;
			var p = path;
			if(path.Length > Application.dataPath.Length + 8 )
				path = "Assets" + path.Substring(Application.dataPath.Length);
			
			Debug.Log("load " + path + "\n "+p + "\n " +Application.dataPath);
			return AssetDatabase.LoadAssetAtPath<T>(path);
		}*/

		public class AssetInfo
		{
			public GameObject AssetLoader;

			public GameObject Asset 
			{
				get{ return _asset; }
				set
				{
					if (_asset != null)
						DestroyImmediate(_asset);
					_asset = value;
					_asset.transform.SetParent(AssetLoader.transform);
				}
			}       
			GameObject _asset;

			public AssetInfo(GameObject assetloader)
			{
				AssetLoader = assetloader;
				if( AssetLoader.transform.childCount > 0)
					Asset = AssetLoader.transform.GetChild(0).gameObject;
			}

			public AssetInfo(Transform container)
			{
				AssetLoader = new GameObject("AssetLoader");
				AssetLoader.transform.position = Vector3.zero;
				AssetLoader.transform.SetParent(container);
			}

			public void OnRemove()
			{
				DestroyImmediate(AssetLoader);
			}
		}

		#region IModalWindow implementation
		public void ModalRequest(bool shift)
		{
			
		}
		public void ModalClosed(ModalWindow window)
		{
			if (window.Result == WindowResult.Ok)
			{
				AssetLoaderModal m = window as AssetLoaderModal;
				Load(m.Name);
			}
		}
		#endregion
	}
}