﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace Runemark.AssetEditor
{
	public class AssetBundleBuilder : EditorWindow
	{
		public static void Toggle(bool t)
	    {
	        var w = (AssetBundleBuilder)EditorWindow.GetWindow(typeof(AssetBundleBuilder));		
	        w.position = new Rect((Screen.width - 250) / 2, (Screen.height - 400) / 2, 500, 400);
			if (!t)
				w.Close();
			else
			{
				w.ShowUtility();
				w.Init();
			}
	    }

		const string PREFAB_PATH = "Assets/_Prefabs/";

		string _savePath = "";
		Dictionary<BuildTarget, bool> _targets = new Dictionary<BuildTarget, bool>()
		{
			{BuildTarget.StandaloneWindows, true},
			{BuildTarget.Android, false},
			{BuildTarget.iOS, false},
		};

		Dictionary<BuildTarget, string> _path = new Dictionary<BuildTarget, string>()
		{
			{BuildTarget.StandaloneWindows, "windows"},
			{BuildTarget.Android, "android"},
			{BuildTarget.iOS, "ios"},
		};



		List<AssetBundleBuild> _bundles = new List<AssetBundleBuild>();

		Vector2 _scrollPos = Vector2.zero;	

		public void Init()
		{
			if (PlayerPrefs.HasKey("RunemarkAssetSavePath"))
				_savePath = PlayerPrefs.GetString("RunemarkAssetSavePath");
		}

		void OnGUI () 
		{
			EditorGUILayout.HelpBox("Asset Bundle Builder", MessageType.None);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.TextField("Save Path: ", _savePath);
			if (GUILayout.Button("...", GUILayout.Width(25)))
			{
				_savePath = EditorUtility.OpenFolderPanel("Browse", "", "");
				PlayerPrefs.SetString("RunemarkAssetSavePath", _savePath);
			}
			EditorGUILayout.EndHorizontal();


			EditorGUILayout.Foldout(true, "Build targets");
			foreach (var k in _targets.Keys)
			{
				var v = EditorGUILayout.Toggle(k.ToString(), _targets[k]);
				if (v != _targets[k])
				{
					_targets[k] = v; 
					return;
				}
			}

			CustomGUI.Splitter();

			_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
			foreach (var b in _bundles)
			{
				EditorGUILayout.Foldout(true, b.assetBundleName + "("+b.assetNames.Length+")");
				foreach (var a in b.assetNames)
					EditorGUILayout.LabelField(a);
			}
			EditorGUILayout.EndScrollView();

			if (GUILayout.Button("Collect"))
				CollectBundlesInfo();

			CustomGUI.Splitter();

			if (GUILayout.Button("Build"))
				Build();


			if (GUILayout.Button("Import assetbundle to edit [not working]"))
			{
			}
		}

		void CollectBundlesInfo()
		{
			_bundles.Clear();

			var info = new DirectoryInfo(PREFAB_PATH);
			var directories = info.GetDirectories();	// These folders name will be the name of a single asset bundle
			foreach (var dir in directories)
			{
				List<string> names = new List<string>();
				foreach (var asset in dir.GetFiles())
				{
					if (!asset.Name.Contains("meta"))
						names.Add(PREFAB_PATH + dir.Name + "/" + asset.Name);                       
				}
				_bundles.Add(new AssetBundleBuild(){ assetBundleName = dir.Name, assetNames = names.ToArray() });
			}
		}


		void Build()
		{		
			if (_bundles.Count == 0)
				CollectBundlesInfo();

			if (!Directory.Exists(_savePath))
				Directory.CreateDirectory(_savePath);
			
			foreach (var t in _targets)
			{				
				if (t.Value)
				{
					var path = _savePath + "/" + _path[t.Key];
					if (!Directory.Exists(path))
						Directory.CreateDirectory(path);

					Debug.Log("Build: " + path);

					BuildPipeline.BuildAssetBundles(path, _bundles.ToArray(), BuildAssetBundleOptions.None, t.Key);
				}
			}
		}
	}

}