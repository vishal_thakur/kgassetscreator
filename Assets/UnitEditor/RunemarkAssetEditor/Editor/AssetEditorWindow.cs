﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;

namespace Runemark.AssetEditor
{
	public class AssetEditorWindow : EditorWindow 
	{
		public static AssetEditorWindow instance;
		public string AssetSavePath;

		bool _initalized;

		public void Init()
		{
			position = new Rect((Screen.width - 100) / 2, (Screen.height - 400) / 2, 200, 400);

			if (PlayerPrefs.HasKey("AppPath"))
				AssetSavePath = PlayerPrefs.GetString("RunemarkAssetSavePath");
			OnInit();
			instance = this;

			_initalized = true;
		}

		void OnGUI () 
		{
			if (!_initalized)
				Init();

			EditorGUILayout.HelpBox("Here comes a logo", MessageType.None);

		/*	EditorGUILayout.BeginHorizontal();
			EditorGUILayout.TextField("Save Path: ", AssetSavePath);
			if (GUILayout.Button("..."))
			{
				AssetSavePath = EditorUtility.OpenFolderPanel("Browse", "", "");
				PlayerPrefs.SetString("RunemarkAssetSavePath", AssetSavePath);
			}
			EditorGUILayout.EndHorizontal();*/

			DrawMenu();

			CustomGUI.Splitter();
			if (GUILayout.Button("Asset Bundle Builder"))
				OpenEditor("AssetBundleBuilder");
		}

		protected void OpenEditor(string editorName)
		{		
			if (editorName != "AssetBundleBuilder")
			{
				if (EditorSceneManager.GetActiveScene().name != editorName)
					EditorSceneManager.OpenScene(scenePath + editorName + ".unity");
				OnEditorOpened(editorName);
			}

			AssetBundleBuilder.Toggle(editorName == "AssetBundleBuilder");		
		}



		protected virtual string scenePath { get { return "unknown"; }}
		protected virtual void OnInit(){}
		protected virtual void DrawMenu(){ }
		protected virtual void OnEditorOpened (string editorName){}	
	}
}