﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace Runemark.Screenshot
{
	public class Screenshot 
	{
		public static void Take(string path, int width, int height, Camera camera = null, bool transparentBackground = false)
		{
			if (camera == null) camera = GameObject.FindGameObjectWithTag("ScreenshotCamera").GetComponent<Camera>();

			width = Screen.width * 4;
			height = Screen.width * 4;

			camera.enabled = true;
			RenderTexture rt = new RenderTexture(width, height, 24);
			camera.targetTexture = rt;
			camera.Render();
			RenderTexture.active = rt;

			TextureFormat tFormat = (transparentBackground) ?  TextureFormat.ARGB32 : TextureFormat.RGB24;
			Texture2D screenShot = new Texture2D(width, height, tFormat, false);
			screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);

			camera.targetTexture = null;
			RenderTexture.active = null; 

			byte[] bytes = screenShot.EncodeToPNG();

			File.WriteAllBytes(path, bytes);
			Debug.Log(string.Format("Took screenshot to: {0}", path));
			Application.OpenURL(path);

			camera.enabled = false;
		}
	}
}