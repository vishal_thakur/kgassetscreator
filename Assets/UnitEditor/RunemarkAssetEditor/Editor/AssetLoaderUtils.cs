﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Runemark.AssetEditor
{
public class AssetLoaderUtils
{
	public static T LoadAsset<T>(string path) where T : Object
	{
		if (path == "") return null;
		var p = path;
		if(path.Length > Application.dataPath.Length + 8 )
			path = "Assets" + path.Substring(Application.dataPath.Length);

		Debug.Log("load " + path + "\n "+p + "\n " +Application.dataPath);
		return AssetDatabase.LoadAssetAtPath<T>(path);
	}


}
}