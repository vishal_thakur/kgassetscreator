﻿namespace Runemark.EditorExtension
{
	public enum WindowResult
	{
		    None,
		    Ok,
		    Cancel,
		    Invalid,
		    LostFocus
	}
}