﻿using UnityEngine;
using System.Collections;

namespace Runemark.EditorExtension
{
	/// <summary>
	/// This EditorWindow can recieve and send Modal inputs.
	/// </summary>
	public interface IModalWindow 
	{
		/// <summary>
	    /// Called when the Modal shortcut is pressed.
	    /// The implementation should call Create if the condition are right.
	    /// </summary>
	    void ModalRequest(bool shift);
	 
	    /// <summary>
	    /// Called when the associated modal is closed.
	    /// </summary>
	    void ModalClosed(ModalWindow window);
	}
}