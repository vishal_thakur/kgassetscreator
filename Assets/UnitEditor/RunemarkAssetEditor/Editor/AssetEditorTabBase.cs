﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Runemark.AssetEditor;

public class AssetEditorTabBase<T> where T : AssetEditorPanel<T>
{
	protected T panel { get; private set; }

	public AssetEditorTabBase(T panel)
	{		
		this.panel = panel;
	}

	public virtual void Draw()
	{
		EditorGUILayout.HelpBox(GetType()+ " editor is not implemented yet.", MessageType.Error);
	}

	public virtual void Save(GameObject root)
	{
		Debug.LogError(GetType() + " doesn't contains save method");
	}

	public virtual void Load(GameObject root)
	{
		Debug.LogError(GetType() + " doesn't contains load method");
	}

	public virtual void Reset()
	{
		Debug.LogError(GetType() + " doesn't contains reset method");
	}

	public virtual void OnAssetInstantiated(Transform transform)
	{
		
	}

	protected bool DrawDictionary_Int_ModelField(Dictionary_Int_GameObject pair)
	{
		if (pair == null) return false;

		GUILayout.BeginHorizontal();
		if (GUILayout.Button("X"))
			return false;

		pair.Value = (GameObject)EditorGUILayout.ObjectField(pair.Value, typeof(GameObject), true);
		pair.Key = EditorGUILayout.IntField(pair.Key);		     

		if(pair.Value != null)
			pair.Value.SetActive(EditorGUILayout.Toggle(pair.Value.activeSelf));

		GUILayout.EndHorizontal();
		return true;
	}
}
