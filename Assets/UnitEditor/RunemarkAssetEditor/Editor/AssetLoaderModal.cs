﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Runemark.EditorExtension;
using System.Collections.Generic;

namespace Runemark.AssetEditor
{
	public class AssetLoaderModal : ModalWindow
	{
		public const int MODELS_IN_ROW = 5;
		public const float BUTTONS_HEIGHT = 20;		  
	    public const float HEIGHT = 56;
	    public const float WIDTH = 500;

		public string Name { get{ return (_selected >= 0 && _selected < _names.Length) ?  _names[_selected] : ""; } }

		int _selected = 0;
		string[] _names;	

		protected override void Draw(Rect region)
		{		
			region.x = region.x + 5;
			region.y = region.y + 5;
			region.width = region.width - 10;
			region.height = region.height - BUTTONS_HEIGHT - 20;

			_selected = GUI.SelectionGrid(region, _selected, _names, MODELS_IN_ROW);
			region.y = region.y + region.height + 5;
			region.height = 1;

			CustomGUI.Splitter(region);
			region.y = region.y + 6;
			region.height = BUTTONS_HEIGHT;
			region.width = region.width / 2;

			if (GUI.Button(region, "Ok"))
				Ok();
			
			region.x = region.x + region.width;

			if (GUI.Button(region, "Cancel"))
				Cancel();			
		}

		public static AssetLoaderModal Create(IModalWindow owner, string title, string[] names)
		{
			AssetLoaderModal assetLoader = AssetLoaderModal.CreateInstance<AssetLoaderModal>();
			 
			assetLoader.owner = owner;
			assetLoader.title = title;

			assetLoader._names = names;

			float halfWidth = WIDTH / 2;
			 		 
			float height = HEIGHT + (names.Length * BUTTONS_HEIGHT) + 20;
			 
			Rect rect = new Rect(0, 0, 0, 0);
			assetLoader.position = rect;
			assetLoader.ShowAsDropDown(rect, new Vector2(WIDTH, height));

			EditorGUIExtensions.CenterOnMainWin(assetLoader);
			 
			return assetLoader;
		}

	}
}