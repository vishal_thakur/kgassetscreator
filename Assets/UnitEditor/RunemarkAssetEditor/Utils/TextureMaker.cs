﻿using UnityEngine;
using System.Collections;

public class TextureMaker
{
    public static Texture2D Create( int width, int height, Color color )
    {
        Color[] pix = new Color[width * height];
        for( int i = 0; i < pix.Length; ++i )
        {
            pix[ i ] = color;
        }
        Texture2D result = new Texture2D( width, height );
        result.SetPixels( pix );
        result.Apply();
        return result;
    }
}
