﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Runemark.AssetEditor
{
	public class AssetBundleLoader
	{
		public static string AssetBundlePath = "";

		public static AssetNames LoadAssetNames(string category)
		{
			AssetNames names = new AssetNames();
			var bundle = LoadBundle(category);
			if (bundle != null)
			{
				var a = bundle.LoadAsset<AssetNames>("assetNames");
				names.Names = a.Names;
				bundle.Unload(true);
			}
			return names;
		}

		public static GameObject GetAsset(string category, string name)
		{
			GameObject asset = null;
			var bundle = LoadBundle(category);
			if (bundle != null)
			{
				asset = bundle.LoadAsset<GameObject>(name);
				bundle.Unload(false);
			}
			return asset;
		}

		public static AssetBundle LoadBundle(string bundleName)
		{
			var filePath = Path.Combine(AssetBundlePath, bundleName);

			// Create the directory if its missing, and prevent errors from this.
			if (!Directory.Exists(AssetBundlePath) || !File.Exists(filePath)) return null;

			var bundle = AssetBundle.LoadFromFile(filePath);
			if (bundle == null)
				Debug.LogError("Loading the assetbundle (" + bundleName + ") on path " + filePath + " failed");
			return bundle;
		}
	}
}